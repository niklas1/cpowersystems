# Synchronous Generator References

## List of models
- only mechanical equations (2nd order)
- one d-axis model (3rd order)
    - one d-axis (Milano) - does not neglect stator winding resistance R
    - 3rd order (Eremia) - neglect winding resistance R
    - Simplified Model with Armortisseurs Neglected (Kundur)
- one d- and one q-axis model (4th order)
    - one d- and one q-axis (Milano) - does not neglect stator winding resistance R
    - 4th order (Eremia) - neglect winding resistance R
- two d- and one q-axis model (5th order)
    - two d- and one q-axis model (Milano)
    - Simulink Model (Synchronous Machine Model 2.1)
    - Simplified VBR model
- two d- and two q-axis model (6th order)
- two d- and two q-axis and transformer voltage (full order)

## List of regulators
 - DC Exciter
    - IEEE type DC1A (Kundur, Eremia) 
    - IEEE 9-bus data sheet
        - transducer included
        - two small variables and saturation neglected
    - Machowski, Chapter 11.2, p. 466
        - two small variables and saturation neglected 
 - AC Exciter (Machowski)
    - IEEE type AC1A (Kundur, Eremia)
    - IEEE type AC4A (Kundur)
    - IEEE type AC5A (Eremia)
    - simplified (Machowski)
 - Static Exciter 
    - IEEE type ST1A (Kundur, Eremia)
    - IEEE type ST2A (Kundur, Eremia)
    - simplified (Machowski)
    
## List of turbine governors 
Steam Turbine:
- Turbine
    - Steam Turbine
        - Tandem Compound 
            - Simplified (IEEE 9-bus data sheet, Kundur, Machowski) 
            - Full Model (Kundur p. 426, Machowski p. 472)
            - Enhanced Model (Kundur p. 431)
        - Cross Compound
            - Generic Model (Kundur p. 430)

- Governor
    - Mechanical-hydraulic
        - Model representing normal speed/load control - with valve limits (Kundur p. 437)
        - Model representing normal speed/load control - with power limit (IEEE 9-bus data sheet)
        - Model representing normal speed/load control (simplified model) - (Machowski p. 475)
        - Model with auxiliary governor (Kundur, Machowski)
    - Electro-hydraulic (Kundur, Machowski)

Hidraulic Turbine:

