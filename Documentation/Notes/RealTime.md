# Optimize execution for real-time

Fedora Script for real-time tuning: https://git.rwth-aachen.de/acs/public/villas/Images/blob/master/files/usr/local/bin/tune-realtime

Isolated cores for real-time sensitive application: https://stackoverflow.com/questions/27990555/isolate-cpu-cores-physical-or-logical#27990628

Verify that you run a RT kernel:
 - `uname -r`
 - `cat /proc/realtime`

Uitilities for starting application:

- `chrt -f 99 your-application`
- `taskset -c 19-23 your-application`

Example:

```
taskset -c 19-23 chrt -f 99 command-you-want-to run
```